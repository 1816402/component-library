import React, { Component } from 'react';
import style from './requestCard.scss';

export interface RequestCardProps {
  imgLink: string;
  imgAlt: string;
  title: string;
  profileName: string;
  viewers: number;
  priceListing: number;
  usersRated: number;
  linkLocation: string;
}

export class RequestCard extends Component<RequestCardProps> {
  render(): JSX.Element {
    const { imgLink, imgAlt, title, profileName, viewers, priceListing, usersRated, linkLocation } = this.props;
    return (
      <div className={style.requestCardContainer}>
        <a href={linkLocation}>
          <div className={style.requestCardImage}>
            <img src={imgLink} alt={imgAlt} />
          </div>
          <div className={style.requestCardContent}>
            <div className={style.requestCardTitleContainer}>
              <p>{title}</p>
            </div>
            <p className={style.cardProfileName}>{profileName}</p>
            <div className={style.requestCardMiddleSection}>
              <div className={style.viwersSection}>
                <i className="fa fa-eye"></i>
                <p>{viewers}</p>
              </div>
              <div className={style.cardPriceListing}>
                <p>S${priceListing}</p>
              </div>
            </div>
            <div className={style.ratingsSection}>
              <p>({usersRated})</p>
            </div>
          </div>
        </a>
      </div>
    );
  }
}
