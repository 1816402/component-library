import React, { Component } from 'react';
import { TextButton } from '../../Atomic/TextButton';
import style from './navBar.scss';

interface NavBarProps {
  navCategories: Array<string>;
}

export class NavBar extends Component<NavBarProps> {
  render(): JSX.Element {
    const { navCategories } = this.props;
    return (
      <div className={style.navMenu}>
        {navCategories.map((value, index) => {
          return (
            <li key={index}>
              <TextButton buttonText={value} linkLocation={'/'} />
            </li>
          );
        })}
      </div>
    );
  }
}
