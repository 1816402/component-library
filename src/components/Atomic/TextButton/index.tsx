import React, { Component } from 'react';
import style from './textButton.scss';

export interface textButtonProps {
  buttonText: string;
  linkLocation: string;
}

export class TextButton extends Component<textButtonProps> {
  render(): JSX.Element {
    const { buttonText, linkLocation } = this.props;

    return (
      <a className={style.textButton} href={linkLocation}>
        {buttonText}
      </a>
    );
  }
}
