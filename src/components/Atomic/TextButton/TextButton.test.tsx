import React from 'react';
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import { TextButton } from './index';

const render: ShallowWrapper = shallow(<TextButton />);

describe('Text Button', function () {
  it('button is rendered', function () {
    const button = render.find('div').find({
      'data-align': 'left',
    });
    expect(button).to.length(1);
  });

  it('render button text', function () {
    const dropdown = render.find('div div div');

    expect(dropdown.text()).to.equal('VIEW MORE');
  });
});
