module.exports = {
    "require": [
        "ts-node/register",
        "jsdom-global/register",
        "source-map-support/register",
        "ignore-styles",
        "./test/helpers.ts",
    ],
    "recursive": true
}